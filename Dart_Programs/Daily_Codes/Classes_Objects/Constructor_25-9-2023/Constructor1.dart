//NULL CLASS - dart having the class with name Null and which is the only one class in dart which Doesnot having parent class is Object Class

class Demo{
	int? x;
	String? str;
	void printData(){
		print(x);
		print(str);
	}
}
void main(){
	Demo obj = new Demo();
	obj.printData();
	
	obj.x = 10;
	obj.str = "Kanha";

	obj.printData();
}
