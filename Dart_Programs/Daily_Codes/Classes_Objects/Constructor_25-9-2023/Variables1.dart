class Demo{
int x =10;
int z =10;
static int y =20;
void printDemo(){
	print(x);
	print(z);
	print(y);//Static Variable can be accessed in the instance method easily & Directly
	//print(this.y); Error-Object through we cannot access the Static variables
}
void printAdd(){
	print(x.hashCode);
	print(z.hashCode);
	print(y.hashCode);
}
}
void main(){
	Demo obj = new Demo();
	obj.printDemo();
	obj.printAdd();
}
