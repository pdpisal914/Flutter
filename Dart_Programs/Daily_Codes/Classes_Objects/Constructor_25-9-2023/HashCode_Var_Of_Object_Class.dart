//HashCode = address or unique id of every object
import 'dart:io';
class Demo{
	int? x;
	String? str;
	
	Demo(int x,String str){
		this.x=10;
		this.str=str;
		
		print(this);
		print(this.hashCode);
	}
}

void main(){
	Demo obj1 = new Demo(10,"Kanha");
		print("obj1 : $obj1");
		stdout.write("HashCode of Obj1 : ");
		stdout.writeln(obj1.hashCode);

	Demo obj2 = new Demo(20,"Ashish");
		print("obj2 : $obj2");
		stdout.write("HashCode of Obj2 : ");
		stdout.writeln(obj2.hashCode);
}
