//Instance variable cannot be accessed from static method but the static variable is accessed from static method inside the class

class Demo{
	int x =10;
	static int y = 20;
	
	static void printData(){
		print(x);//Error-Instance variable cannot be accessed from static context
		print(y);
	}
}
void main(){
	
}
