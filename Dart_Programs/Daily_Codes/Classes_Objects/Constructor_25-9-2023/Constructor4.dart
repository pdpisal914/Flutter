//Instance variable is placed inside the Constructor

class Demo{
	int? x;
	String? str;
	
	Demo(int x,String str){
		this.x=x;
		this.str=str;
		print("In Parameterized constructor");
	}
	
	void printData(){
		print(x);
		print(str);
	}
}
void main(){
	Demo obj = new Demo(10,"Kanha");
		obj.printData();
}
