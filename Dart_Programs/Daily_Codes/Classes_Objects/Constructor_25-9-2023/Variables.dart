//Static/Class Variable cannot be Accessed by using Object of Class
//Static variable can be accessed by using class name
class Demo {
  int x = 10;
  static int y = 20;
}

void main() {
  Demo obj = new Demo();
  print(obj.x);
 // print(obj.y);//Static Variable Cannot be Accessed with the help of object
//Static variables of class are accessed only by using class Name
	print(Demo.y);
}
