//Instance variable is placed inside the Constructor

class Demo{
	int? x;
	String? str;
	
	Demo(int var1,String name){
		print("In Parameterized constructor");
	}
	
	void printData(){
		print(x);
		print(str);
	}
}
void main(){
	Demo obj = new Demo(10,"Kanha");
		obj.printData();
}
