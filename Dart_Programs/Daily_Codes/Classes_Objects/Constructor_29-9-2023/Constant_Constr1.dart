class Demo{
	final int? x;
	final String? str;
	const Demo(this.x,this.str);
}
void main(){
	Demo obj1 = const Demo(10,"Ashish");
	print(obj1.hashCode);//1000
	Demo obj2 = const Demo(10,"Ashish");
	print(obj2.hashCode);//1000
	Demo obj3 = const Demo(20,"Kanha");
	print(obj3.hashCode);//2000
}
