class C{
String? founder;
int? year;

void syntax(){
print("Vey Complex Syntax");
}
void structure(){
print("OIn Structure");
}

C(this.founder,this.year);
}

class Java extends C{
String? founder ;
int? year;

Java(this.founder,this.year,String x,int y) : super(x,y);

void Classes(){
print("Classes And Objects");
}

void syntax(){
print("Simple & Easy");
}
}
void main(){
Java obj = new Java("Dennis Ritchi",1972,"James Gosling",1995);
obj.syntax();
obj.Classes();
}
