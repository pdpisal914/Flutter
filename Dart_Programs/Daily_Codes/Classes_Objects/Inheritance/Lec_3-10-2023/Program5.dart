class Parent{
	int x =10;
	String str = "Pranav";
	
	Parent(){
		print("IN Parent Constructor");
	}

	void parentMethod(){
		print(x);
		print(str);
	}
}

class Child extends Parent{
	int x =20;
	String str = "Pisal";
	
	Child(){
		print("In Child Constructor");
	}
	
	void childMethod(){
		print(x);
		print(str);
	}
}
void main(){

	Parent obj1 = new Parent();
	Child obj2 = new Child();
	
	print(obj2.x);
	print(obj2.str);
	obj2.parentMethod();
}
