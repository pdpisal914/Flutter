//Static methods are not accessible by using the object of that class,the Static variables and the static methods are 
//accessible only by using the Name of the Class

//the Instance variables or the Instance Methods are not accessible / visible from the static context

class Parent{
	int x =10;
	String str = "Pranav";

	void parentMethod(){
		print(x);
		print(str);
	}
}
class Child extends Parent{
	int y = 20;
	String str1 = "Pisal";

	void childMethod(){
		print(y);
		print(str1);
	}
}
void main(){
	Parent obj = new Parent();
	print(obj.x);
	print(obj.str);
	obj.parentMethod();

	Child obj1 = new Child();
	print(obj1.x);
	print(obj1.str);
	obj1.parentMethod();
	print(obj1.y);
	print(obj1.str1);
	obj1.childMethod();

	
}
	

