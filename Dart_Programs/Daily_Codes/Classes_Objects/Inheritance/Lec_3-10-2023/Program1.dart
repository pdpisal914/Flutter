//Error=Childs variable or methods are not accessed by using the Parent Reference/object


class Parent{
	int x =10;
	String str = "Pranav";

	void parentMethod(){
		print(x);
		print(str);
	}
}
class Child extends Parent{
	int y = 20;
	String str1 = "Pisal";

	void childMethod(){
		print(y);
		print(str1);
	}
}
void main(){
	Parent obj = new Parent();
	print(obj.y);
	print(obj.str1);
	print(obj.childMethod());
}
	

