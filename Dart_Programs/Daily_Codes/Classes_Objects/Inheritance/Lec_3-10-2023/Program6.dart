//Static methods are not accessible by using the object of that class,the Static variables and the static methods are 
//accessible only by using the Name of the Class

//the Instance variables or the Instance Methods are not accessible / visible from the static context

class Parent{
	static int x =10;
	static String str = "Pranav";

	static void parentMethod(){
		print(x);
		print(str);
	}
}
class Child extends Parent{
	int y = 20;
	String str1 = "Pisal";

	void childMethod(){
		print(y);
		print(str1);
	}
}
void main(){

	Child obj1 = new Child();
	/*print(Child.x);
	print(Child.str);  All give error-static methods of parent class are not accessed by the child class Name
	Child.parentMethod();*/
	
	print(Parent.x);
	print(Parent.str);
	Parent.parentMethod();

	print(obj1.y);
	print(obj1.str1);
	obj1.childMethod();

	
}
	

