//Static methods are not accessible by using the object of that class,the Static variables and the static methods are 
//accessible only by using the Name of the Class

//the Instance variables or the Instance Methods are not accessible / visible from the static context

class Parent{
	int x =10;
	String str = "Pranav";

	void parentMethod(){
		print(x);
		print(str);
	}
}
class Child extends Parent{
	int x = 20;
	String str = "Pisal";

	void childMethod(){
		print(x);
		print(str);
	}
}
void main(){

	Child obj1 = new Child();
	print(obj1.x);
	print(obj1.str);
	obj1.parentMethod();
	print(obj1.x);
	print(obj1.str);
	obj1.childMethod();

	
}
	

