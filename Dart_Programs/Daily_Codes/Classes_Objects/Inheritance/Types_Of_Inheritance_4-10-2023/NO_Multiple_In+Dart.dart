//Multiple Inheritance is not present in Dart

class Parent1{
	Parent1(){
		print("In Parent1");
	}
}
class Parent2{
	Parent2(){
		print("In Parent2");
	}
}
class Child extends Parent1,Parent2{
	Child(){
		print("In Child Constructor");
	}
}
void main(){
	Child obj = new Child();
}
