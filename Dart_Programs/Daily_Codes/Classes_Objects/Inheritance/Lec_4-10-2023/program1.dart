class Parent{
	int x =10;
	Parent(){
		print("In Parent Constructor");
	}
	
	void printData(){
		print(x);
	}
}
class Child extends Parent{
	int x =20;
	Child(){
		print("In Child Constructor");
	}
	void displayData(){
		print(x);
	}
}

void main(){
	Parent p = new Parent();
	Child obj = new Child();
	obj.printData();
	obj.displayData();
}
