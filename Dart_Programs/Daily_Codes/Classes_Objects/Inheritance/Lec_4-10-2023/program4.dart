class Parent{
	int x =10;
	Parent(){
		print("In Parent Constructor");
	}
	
	void printData(){
		print(x);
	}
	call(){
		print("In Call Method");
		this.printData();
	}
}
class Child extends Parent{
	int x =20;
	Child(){
		super();
		print("In Child Constructor");
	}
	void displayData(){
		print(x);
	}
}

void main(){
	Child obj = new Child();
	obj.printData();
	obj.displayData();
	obj();
}

//ERROR :- super() asa apn parent chya conbstructor la call nhi deu shakat bcoz the super is the object of the Parent and the super is not
//Callable Object hence we are not able to call the constructor of parent class like super()
//if we want to give the call like super() OR if we want to make the object is callable then we required to define the method of name "call()" in parnet 
//class or in the Self class 
//when we write the method "call()" in the parent class then the super object makes the callable object
