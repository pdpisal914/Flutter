//Error= Constant Constructyor cannot have a body

class Player{
	final int? no;
	final String? name;
	
	/*const Player(this.no,this.name){
		print(no);
		print(name);
		print("In Parameterized Constructor");
	}*/
	const Player(this.no,this.name);
}
void main(){
	Player p1 = new Player(100,"Pranav");
}
