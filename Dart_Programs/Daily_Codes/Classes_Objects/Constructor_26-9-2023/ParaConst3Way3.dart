//Parametric Constructor 
//Way 3 = Default Parameters

class Company{
	int? cnt;
	String? name;

	Company(this.cnt,{this.name="Biencaps"});
		
	void compInfo(){
		print(cnt);
		print(name);
	}
}
void main(){
	Company obj = new Company(1000);
	Company obj1 = new Company(25);
	
//	Company obj2 = new Company(200,"PubMatic");//Error
	
	obj.compInfo();
	obj1.compInfo();
	//obj2.compInfo();
}
