//Parametric Constructor 
//Way 4 = Named Parameters

class Company{
	int? cnt;
	String? name;

	Company({this.cnt,this.name="Biencaps"});
		
	void compInfo(){
		print(cnt);
		print(name);
	}
}
void main(){
	Company obj1 = new Company(cnt:25,name:"Veritas");
	
	Company obj2 = new Company(name:"PubMatic",cnt:5343);
	
	obj1.compInfo();
	obj2.compInfo();
}
