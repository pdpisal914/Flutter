abstract class Demo{
Demo(){
print("Demo constructor");
}
void fun1(){
print("In Fun1");
}

void fun2();
}

class Child extends Demo{
Child(){
print("In Child Constructor");
}

void fun2(){
print("In fun2");
}
}

void main(){
Child c = new Child();
c.fun1();
c.fun2();
}
