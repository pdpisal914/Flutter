abstract class Developer{
Developer(){
print("In Parent Constructor");
}
void develop();
}
class mobDev extends Developer{
MobDev(){
print("In ModDeveloper child");
}

void develop(){
print("Flutter Developer");
}
}
class webDev extends Developer{
webDev(){
print("In webDev Constructor");
}
void develop(){
print("Java Developer");
}
}


void main(){
Developer obj = new mobDev();
obj.develop();

Developer obj1 = new webDev();
obj1.develop();

webDev obj2 = new webDev();
obj2.develop();

Developer obj5 = new Developer();
obj5.develop();
}



