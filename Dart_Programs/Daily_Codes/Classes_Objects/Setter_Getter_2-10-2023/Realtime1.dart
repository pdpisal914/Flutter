class RBI{
	void rbi(){
		print("Global Rules Of RBI For All Banks");
	}
}
class SBI extends RBI{
	void sbi(){
		print("Rules Of RBI is Inherited By SBI");
	}
}
class HDFC extends RBI{
	void hdfc(){
		print("Rules Of RBI Is Inherited By HDFC");
	}
}
void main(){
	SBI b1 = new SBI();
	b1.rbi();
	b1.sbi();

	HDFC b2 = new HDFC();
	b2.rbi();
	b2.hdfc();
}
