//Setter Method
//Way 1

class Demo{
	int? _x;
	String? str;
	double? _sal;

	Demo(this._x,this.str,this._sal);

	void set setX(int x){
		_x = x;
	}
	
	void set setSal(double sal){
		_sal=sal;
	}
	
	void display(){
		print(_x);
		print(str);
		print(_sal);
	}
}
