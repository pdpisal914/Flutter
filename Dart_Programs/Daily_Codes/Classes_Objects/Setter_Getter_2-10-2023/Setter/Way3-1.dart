//Setter Method
//Way 1
//setter method with set keyword 
//Easy Way

class Demo{
	int? _x;
	String? str;
	double? _sal;

	Demo(this._x,this.str,this._sal);

	set setX(int x) => _x = x;
	
	set setSal(double sal) => _sal=sal;
	
	void display(){
		print(_x);
		print(str);
		print(_sal);
	}
}
