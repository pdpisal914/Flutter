//Getter Methods
//Way 2

class Demo{
	int? _x;
	String? str;
	double? _sal;
	
	Demo(this._x,this.str,this._sal);
	
	int? get getX{//without ()
		return _x;
	}
	
	double? get getSal{//without ()
		return _sal;
	}
}
