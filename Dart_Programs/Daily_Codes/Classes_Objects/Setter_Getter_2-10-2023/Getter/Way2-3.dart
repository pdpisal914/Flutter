//Getter Methods
//Way 2-Without Return Type

class Demo{
	int? _x;
	String? str;
	double? _sal;
	
	Demo(this._x,this.str,this._sal);
	
	get getX{//without ()
		return _x;
	}
	
	get getSal{//without ()
		return _sal;
	}
}
