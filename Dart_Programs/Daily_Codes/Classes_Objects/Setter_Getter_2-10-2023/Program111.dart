

class Demo{
	int? _x;
	String? str;
	double? _sal;

	Demo(this._x,this.str,this._sal);

	set setX(int x)=>_x=x;
	set setName(String str)=>this.str=str;
	set setSal(double sal)=>_sal=sal;

	int? getX(){
		return _x;
	}
	String? getName(){
		return str;
	}
	double? getSal(){
		return _sal;
	}

	void display(){
		print(_x);
		print(str);
		print(_sal);
	}
}
