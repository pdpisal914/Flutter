

class Demo{
	int? _x;
	String? str;
	double? _sal;

	Demo(this._x,this.str,this._sal);

	set setX(int x)=>_x=x;
	set setName(String str)=>this.str=str;
	set setSal(double sal)=>_sal=sal;

	get getX => _x;
	get getName => str;
	get getSal => _sal;

	void display(){
		print(_x);
		print(str);
		print(_sal);
	}
}
