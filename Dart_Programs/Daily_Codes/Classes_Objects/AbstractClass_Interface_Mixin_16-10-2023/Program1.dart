abstract class IFC {
  void material() {
    print("Indian Material");
  }

  void taste();
}

class IndianFC implements IFC {
  void material() {
    print("Indian Material");
  }

  void taste() {
    print("Indian Taste");
  }
}

class EUFC implements IFC {
  void material() {
    print("Indian Material");
  }

  void taste() {
    print("SEUFC Taste");
  }
}

void main() {
  IndianFC obj1 = new IndianFC();
  obj1.material();
  obj1.taste();

  EUFC obj2 = new EUFC();
  obj2.material();
  obj2.taste();
}
