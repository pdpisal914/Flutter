mixin Parent1 {
  void m1() {
    print("In m1 Parent1");
  }
}
mixin Parent2 {
  void m1() {
    print("In m1 Parent2");
  }
}

class Child with Parent1, Parent2 {}

void main() {
  Child obj = new Child();
  obj.m1();
}
