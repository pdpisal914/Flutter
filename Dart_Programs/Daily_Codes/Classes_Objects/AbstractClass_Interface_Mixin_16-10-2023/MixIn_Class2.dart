mixin Parent1 {
  void m1() {
    print("In m1-Parent");
  }
}

class Parent2 {
  void m1() {
    print("In m1 Parent2");
  }
}

class Demo extends Parent2 with Parent1 {}

void main() {
  Demo obj = new Demo();
  obj.m1();
}
