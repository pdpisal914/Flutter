abstract class InterfaceDemo1{
	int x =10;
	void m1(){
		print("In m1-InterfaceDemo1");
	}
}
abstract class InterfaceDemo2{
	int x =20;
	void m2(){
		print("In m2-InterfaceDemo2");
	}
}
class Demo implements InterfaceDemo1,InterfaceDemo2{

	int x =30;
	void m1(){
		print("In m1 Demo");
	}
	void m2(){
		print("In m2 Demo");
	}
}
void main(){
	Demo obj = new Demo();
	obj.m1();
	obj.m2();

	print(obj.x);
}
