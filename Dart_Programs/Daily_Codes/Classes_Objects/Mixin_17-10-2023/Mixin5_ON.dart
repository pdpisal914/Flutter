mixin Demo1 {
  void fun1() {
    print("In Fun1");
  }
}

mixin Demo2 {
  void fun2() {
    print("In Fun2");
  }
}

class Demo extends Object with Demo1, Demo2 {}

void main() {
  Demo obj = new Demo();
  obj.fun();
  obj.fun2();
}
