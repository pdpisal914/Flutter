class Parent {
  void m1() {
    print("In m1");
  }
}

mixin Demo1 on Parent {
  void fun1() {
    print("In fun1 Demo1");
  }
}

class Normal extends Parent with Demo1 {}

void main() {
  Normal obj = new Normal();
}
