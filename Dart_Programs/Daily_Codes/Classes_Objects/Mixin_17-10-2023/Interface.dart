abstract class Demo {
  void fun1() {
    print("In Fun1 Demo");
  }

  void fun2();
}

abstract class Demo2 {
  void fun3() {
    print("In Fun3 Demo2");
  }

  void fun4();
}

class DemoChild implements Demo1, Demo2 {
  void fun2() {
    print("In Fun2 Child");
  }

  void fun4() {
    print("In Fun4 Child");
  }
}

void main() {
  DemoChild obj = new DemoChild();
  obj.fun1();
  obj.fun2();
  obj.fun3();
  obj.fun4();
}
