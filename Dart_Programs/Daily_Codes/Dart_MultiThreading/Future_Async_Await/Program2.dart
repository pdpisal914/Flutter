import 'dart:io';
Future<String> getOrder(){
	return Future.delayed(Duration(seconds:5),()=>"Burger");
}

Future<String> getOrderMsg() async{
	var order = await getOrder();
	return "Your Order is $order";
}

Future<void> main() async{
	print("Start");
	
	print(await getOrderMsg());
	
	print("End");
}
