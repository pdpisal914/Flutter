import 'dart:io';

String? placeOrder(){
	String? str = stdin.readLineSync();
	return str;
}

Future<String?> getOrder() async{
	return Future.delayed(Duration(seconds:5),()=>placeOrder());
}

Future<String?> getOrderMsg() async{
	var order = await getOrder();
	return "Your Order Is $order";
}

void main(){
	print("Start");
	
	var order = getOrderMsg();
	
	order.then((val)=>print(val));

	order.then((val)=>print("End"));
}
