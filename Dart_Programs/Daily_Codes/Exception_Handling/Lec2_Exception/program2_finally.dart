import 'dart:io';
void main(){
	int? x;
	
	print("Start Code");
	try{
		print("Connection Open");
		x = int.parse(stdin.readLineSync()!);//if exception is occured at this line then the below lines are not executed
		print(x);
		print("Connection Close");
	}on FormatException{
		print("Format Exception is Occurs");
	}on IntegerDivisionByZeroException{
		print("Don't Divide any number by zero");
	}catch(ex){
		print(ex);
	}
	print("End code");
}
