import 'dart:io';
void main(){
	int? x;
	print("Start Code");

	try{	
		print("Connection Opened");
		x = int.parse(stdin.readLineSync()!);
		print(x);
		print("Connection Closed");
	}on FormatException{
		print("Format Exception Is Occured");
	}on IntegerDivisionByZeroException{
		print("Don't Divide Any Number By Zero");
	}catch(ex){
		print(ex);
	}finally{
		print("Connection Closed");
	}
	print("End Code");
}


