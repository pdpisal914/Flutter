class Demo{
	void fun(){
		print("In Fun");
	}
}

void main(){
	Demo? obj = new Demo();
	obj.fun();
	obj = null;
	obj.fun();
}
