//WAP to print square of odd digits between 20 to 10
void main() {
  int high = 20;
  int low = 10;
  while (high >= low) {
    if (high % 2 != 0) {
      print(high * high);
    }
    high--;
  }
}
