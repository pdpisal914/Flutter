//WAP to print product of odd digittd between 10 to 1
void main() {
  int low = 1;
  int high = 10;
  int mult = 1;
  while (high >= low) {
    if (high % 2 != 0) {
      mult = mult * high;
    }
    high--;
  }
  print(mult);
}
