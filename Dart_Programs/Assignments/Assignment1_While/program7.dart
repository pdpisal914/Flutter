//WAP to print the square of even digits and cube of odd digits
void main() {
  int high = 50;
  int low = 40;
  while (low <= high) {
    if (low % 2 != 0) {
      print(low * low);
    } else {
      print(low * low * low);
    }
    low++;
  }
}
