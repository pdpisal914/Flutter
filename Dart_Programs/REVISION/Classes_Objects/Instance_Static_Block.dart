class Demo{
/*	{
		print("In Instance Block");
	}
*/		
	Demo(){
		print("In Demo Constructor");
	}
/*	
	static{
		print("In Static Block");
	}
*/	
	void info(){
		print("In Info");
	}
}
void main(){
	Demo obj = new Demo();
	obj.info();
}
//Their is no any Instance and static block is present in Dart	
