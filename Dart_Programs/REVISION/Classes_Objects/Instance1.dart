import 'dart:io';
class Employee{
	int? id = 1;
	String? name = "Pranav Pisal";
	double? sal = 87.8;

	
	void printInfo(){
		print(id);
		print(name);
		print(sal);
	}
}
void main(){
	Employee emp = new Employee();
	emp.printInfo();

	Employee emp1 = Employee();
	emp1.printInfo();
	emp1.id = 2;
	emp1.name = "Rohit Lahmate";
	emp1.sal = 90.0;

	emp1.printInfo();
	emp.printInfo();
}
