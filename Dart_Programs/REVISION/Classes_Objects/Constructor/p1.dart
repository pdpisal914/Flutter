class Company{
	int? empCount ;
	String? comp;
	
	Company(this.empCount,this.comp);

	void info(){
		print(this.empCount);
		print(this.comp);
	}
}
void main(){
	Company cmp1 = new Company(10,"Pubmatic");
	Company cmp2 = new Company(29,"Veritas");
	
	cmp1.info();
	cmp2.info();
}
