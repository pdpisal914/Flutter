class Company{
	int? empCount =70000;
	String? comp = "Microsoft";
	
	Company({this.empCount,this.comp});

	void info(){
		print(this.empCount);
		print(this.comp);
	}
}
void main(){
	Company cmp1 = new Company(empCount:10,comp:"Pubmatic");
	Company cmp2 = new Company(comp:"Veritas",empCount:29);
	
	cmp1.info();
	cmp2.info();
}
