import 'dart:io';
class Employee{
	static int? id = 1;
	static String? name = "Pranav Pisal";
	static double? sal = 87.8;

	
	void printInfo(){
		print(id);
		print(name);
		print(sal);
	}
}
void main(){
	Employee emp = new Employee();
	emp.printInfo();

	Employee emp1 = Employee();
	emp1.printInfo();
	
	/*
	emp1.id = 2;//Object chya help ne apn Static things Access kru shkt nahi
	emp1.name = "Rohit Lahmate";
	emp1.sal = 90.0;
	*/

	Employee.id = 2;
	Employee.name = "Rohit Lahamte";
	Employee.sal = 90.0;

	emp1.printInfo();
	emp.printInfo();
}
