import 'dart:io';
class Employee{
	int? id = 1;
	String? name = "Pranav Pisal";
	double? sal = 87.8;
	
	void changeInfo(){
		print("Enter the empId");
		id = int.parse(stdin.readLineSync()!);

		print("Enter The Employee Name");
		name = stdin.readLineSync();
		
		print("Enter The Salary");
		sal = double.parse(stdin.readLineSync()!);
	}
	
	void printInfo(){
		print(id);
		print(name);
		print(sal);
	}
}
void main(){
	Employee emp = new Employee();
	emp.printInfo();

	emp.changeInfo();
	
	emp.printInfo();
}
