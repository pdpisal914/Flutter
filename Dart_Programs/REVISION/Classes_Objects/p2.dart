class Employee{
	String? emp;
	double? sal;
	
	Employee(String emp,double sal){
		this.emp=emp;
		this.sal=sal;
	}
	
	void printInfo(){
		print(emp);
		print(sal);
	}
}
void main(){
	Employee emp = Employee("Pranav Pisal",87.5);
	emp.printInfo();
	print(emp.emp);
	print(emp.sal);
	//Class Name
	
	//print(Employee.emp); //Member not found emp
	//print(Employee.sal);//Member not found
}
