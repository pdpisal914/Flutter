class Company{
	String? cname;
	int? empCount;
	
	Company(String? cname , int? empCount){
		this.cname = cname;
		this.empCount = empCount;
	}
	void printInfo(){
		print(this.cname);
		print(this.empCount);
		print(this);
		print(this.runtimeType);
	}
}
void main(){	
	Company cmp = new Company("Microsoft",76889);
	cmp.printInfo();
	print(cmp);
	print(cmp.runtimeType);

}
